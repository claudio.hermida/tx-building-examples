module apollo-server

go 1.21.4

require (
	github.com/Salvionied/apollo v1.0.5
	github.com/blinklabs-io/bursa v0.5.1
)

require (
	github.com/Salvionied/cbor/v2 v2.6.0 // indirect
	github.com/btcsuite/btcutil v1.0.2 // indirect
	github.com/fivebinaries/go-cardano-serialization v0.0.0-20220907134105-ec9b85086588 // indirect
	github.com/fxamacker/cbor/v2 v2.5.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/tyler-smith/go-bip39 v1.1.0 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
