package adminWallet

import (
	"github.com/blinklabs-io/bursa"
)

var adminWallet = &bursa.Wallet{}

func Setup() {
	// Setup wallet
	mnemonic := "SEED_IS_HERE"
	rootKey, err := bursa.GetRootKeyFromMnemonic(mnemonic)
	if err != nil {
		panic(err)
	}
	accountKey := bursa.GetAccountKey(rootKey, 0)
	paymentKey := bursa.GetPaymentKey(accountKey, 0)
	stakeKey := bursa.GetStakeKey(accountKey, 0)
	addr := bursa.GetAddress(accountKey, "preprod", 0)
	wallet := &bursa.Wallet{
		Mnemonic:       mnemonic,
		PaymentAddress: addr.String(),
		StakeAddress:   addr.ToReward().String(),
		PaymentVKey:    bursa.GetPaymentVKey(paymentKey),
		PaymentSKey:    bursa.GetPaymentSKey(paymentKey),
		StakeVKey:      bursa.GetStakeVKey(stakeKey),
		StakeSKey:      bursa.GetStakeVKey(stakeKey),
	}
	adminWallet = wallet
}

func GetWallet() *bursa.Wallet {
	return adminWallet
}
