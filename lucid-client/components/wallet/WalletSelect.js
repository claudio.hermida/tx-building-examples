// import { Blockfrost, Constr, Data, Lucid, Type, fromText } from "lucid-cardano";

import { makeHexID, returnStakeAddressFromBech32, sleep } from "../../lib/base";
import { requestJWT, requestTokenCount, requestWhitelistCheck } from "../../lib/fetch";
import { useEffect, useState } from "react";

import Loading from "../Loading";
import Status from "../Status";
import WalletInfo from "./WalletInfo";
import WalletSelection from "./WalletSelection";
import { list } from "postcss";

// import { BrowserWallet } from "@meshsdk/core";

export default function WalletSelect({
    funds,
    setFunds,
    statusMessage,
    setMessage,
    loading,
    setLoading,
    lucid,
    setLucid,
    api,
    setAPI,
    selectedWallet,
    setSelectedWallet,
    setJWT,
}) {
    const [loadingMessage, setLoadingMessage] = useState();

    useEffect(() => {
        (async () => {
            // const frontEndLucid = await Lucid.new(
            //     new Blockfrost(process.env.api_url, process.env.api_key),
            //     process.env.api_net,
            // );
            // setLucid(frontEndLucid);

            // const backEndLucid = await Lucid.new(
            //     new Blockfrost(process.env.api_url, process.env.api_key),
            //     process.env.api_net,
            // );

            if (selectedWallet != null) {
                setLoading(true);
                setLoadingMessage("selecting wallet");
                const api = await selectedWallet.enable().catch((err) => {
                    console.log(err);

                    if (err.info) setMessage(err.info, "error");
                    else if (err.message) setMessage(err.message, "error");
                    else setMessage(JSON.stringify(err), "error");

                    return null;
                });

                if (!api) {
                    reset();
                    return;
                }
                setMessage(`loading...`, "update");

                const networkId = await api.getNetworkId();
                if (networkId != process.env.network) {
                    setMessage(`Wallet is not on network!`, "error");
                    reset();
                    return;
                }

                // try {
                //     await frontEndLucid.selectWallet(api);
                // } catch (error) {
                //     handleError(error);
                // }

                // const wallet = await BrowserWallet.enable(selectedWallet.name);

                // let verified = await verifyWallet()

                // if (!verified) {
                //   setMessage(`Signature not verified!`, "error")
                //   reset()
                //   return
                // }

                // const utxos = await frontEndLucid.wallet.getUtxos();
                // const utxos = await api.getUtxos();

                // const walletAda = getTotalAda(utxos);
                // setFunds(walletAda);

                setAPI(api);

                await sleep(1000);
                setLoading(false);

                //! ----------------------------------------------------------------------

                // const apolloCBOR =
                //     "84a500818258205200dae6e2ea1ec943a263bc29d82a86f6db7ab0cbb76048853a12b1607fb032020183825839008bdf956671694356e11a669fedf30759f373568dd25f4b510bcd590195c1a4504cdcf94cc88d045cb5f99c581f3b638e5ae0c896848a9cc51a08f0d18082583900c408ec82820b576b7a15b98c440557d954f2667973e633015eafe050af4324e5bd3b36ff582e1b18776c90b78977dd7dfd30f70055b926ce1a0a21fe80825839003303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92c44b606f69e387098b206e20c78ccfb167eaf42826ce81697e84fb781abecdeb70021a0003226d031a02b7bd7d0e82581c3303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92581c5f8c24ba455f9ad1cbc14e8acd8d30d06735c47e2a4dfc202a0235c2a20081825820f48c1363aa87e177789dead3cc9c354ee4675e8c53f560e5eedb587ebb4fc3b458406a3060939eb3db14a1ebd38c4eb7407e418196eb61133035ba1cd93f28b6bfbc0aa156a0dfc322936ce35c6472eba6e20c54eaa35438984124c0b5e92708b002049ffff5f6";
                // const signedTx = await api.signTx(apolloCBOR, true);
                // console.log(signedTx);
                // const txHash = await api.submitTx(signedTx);
                // console.log(txHash);

                //! -----------------------------------------------------------------------

                // const learnerMintingRefUTxO = await backEndLucid.utxosByOutRef([
                //     {
                //         txHash: "bc127504dd27a79b3c1321f5a78ba858477cc2b7695704f372976b3f1e9c176b",
                //         outputIndex: 0,
                //     },
                // ]);

                // const learnerMintingScriptAddress = backEndLucid.utils.validatorToAddress(
                //     learnerMintingRefUTxO[0].scriptRef,
                // );

                // const learnerReferenceRefUTxO = await backEndLucid.utxosByOutRef([
                //     {
                //         txHash: "1246b4ab6f5ecad576a1ae6e07d17c8f9db4a3a81983d81d4c77e72616b0d4b6",
                //         outputIndex: 1,
                //     },
                // ]);

                // const learnerReferenceAddress = backEndLucid.utils.validatorToAddress(
                //     learnerReferenceRefUTxO[0].scriptRef,
                // );

                // const userAddr = await frontEndLucid.wallet.address();
                // const { paymentCredential: userPaymentCredential, stakeCredential: userStakeCredential } =
                //     frontEndLucid.utils.getAddressDetails(userAddr);

                // const learnerTokenPolicyId = backEndLucid.utils.mintingPolicyToId(learnerMintingRefUTxO[0].scriptRef);
                // const learnerTokenTokenNameRed = fromText("BuiltByLucid");
                // const learnerTokenTokenNameToLearnerReferenceScript = fromText("100BuiltByLucid");
                // const learnerTokenTokenNameToUser = fromText("222BuiltByLucid");
                // const learnerTokenToUser = learnerTokenPolicyId + learnerTokenTokenNameToUser;
                // const learnerTokenToContract = learnerTokenPolicyId + learnerTokenTokenNameToLearnerReferenceScript;

                // const learnerInfo = fromText("In_this_case_it_is_just_a_string");

                // const SEED =
                //     "SEED_IS_HERE";
                // backEndLucid.selectWalletFromSeed(SEED);
                // const adminAddr = await backEndLucid.wallet.address();
                // const adminDetails = backEndLucid.utils.getAddressDetails(adminAddr);
                // const adminPKH = adminDetails.paymentCredential.hash;

                // const userUtxos = await backEndLucid.utxosAt(userAddr);

                // // const emptyList = Data.Object([]);
                // const emptyList = [Data.empty()];

                // const learnerReferenceDatum = Data.to(
                //     new Constr(0, [
                //         new Constr(0, [new Constr(0, [userPaymentCredential.hash]), new Constr(1, [])]),
                //         emptyList,
                //         learnerInfo,
                //         learnerTokenPolicyId,
                //         emptyList,
                //     ]),
                // );

                // console.log(learnerReferenceDatum);

                // const learnerMintingRedeemer = Data.to(
                //     new Constr(0, [
                //         new Constr(0, [
                //             new Constr(0, [
                //                 new Constr(0, [userPaymentCredential.hash]),
                //                 new Constr(0, [new Constr(0, [new Constr(0, [userStakeCredential.hash])])]),
                //             ]),
                //             learnerTokenTokenNameRed,
                //             learnerInfo,
                //         ]),
                //     ]),
                // );

                // const timeValidityOfSingingTx = 3 * 60 * 1000;

                // setMessage(`Transaction Building. Please wait ...`, "update");

                // const tx = await backEndLucid
                //     .newTx()
                //     .collectFrom(userUtxos)
                //     .readFrom(learnerMintingRefUTxO)
                //     .mintAssets({ [learnerTokenToUser]: 1n }, learnerMintingRedeemer)
                //     .mintAssets({ [learnerTokenToContract]: 1n }, learnerMintingRedeemer)
                //     .payToAddress(userAddr, { [learnerTokenToUser]: 1n })
                //     .payToContract(
                //         learnerReferenceAddress,
                //         { inline: learnerReferenceDatum },
                //         { [learnerTokenToContract]: 1n },
                //     )
                //     .addSignerKey(userPaymentCredential.hash)
                //     .addSignerKey(adminPKH)
                //     .validTo(Date.now() + timeValidityOfSingingTx)
                //     .complete({coinSelection: false, change: userAddr});

                // const adminSigningTx = await tx.partialSign();
                // const txBuiltAtBackendConvertToCBOR = await tx.toString();
                // console.log("adminSigningTx: ", adminSigningTx);
                // console.log("txBuiltAtBackendConvertToCBOR: ", txBuiltAtBackendConvertToCBOR);

                // const apolloCBOR =
                //     "84a50081825820778b3f0c2ac86f48d75482309a13528bd0d1ca258e164288e45df864db702bdc020183825839008bdf956671694356e11a669fedf30759f373568dd25f4b510bcd590195c1a4504cdcf94cc88d045cb5f99c581f3b638e5ae0c896848a9cc51a08f0d18082583900c408ec82820b576b7a15b98c440557d954f2667973e633015eafe050af4324e5bd3b36ff582e1b18776c90b78977dd7dfd30f70055b926ce1a0a21fe80825839003303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92c44b606f69e387098b206e20c78ccfb167eaf42826ce81697e84fb781ad1e404e1021a0002fb69031a02b7a1c90e81581c3303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92a1049ffff5f6";

                // const serializedCBOR = await frontEndLucid.fromTx(apolloCBOR);

                // const signedTx = await serializedCBOR.sign().complete();
                // const txhash = await signedTx.submit();
                // console.log(txhash);
                // const result = await frontEndLucid.awaitTx(txhash);
                // console.log(result);

                // const apolloCBOR =
                //     "84a50081825820778b3f0c2ac86f48d75482309a13528bd0d1ca258e164288e45df864db702bdc020183825839008bdf956671694356e11a669fedf30759f373568dd25f4b510bcd590195c1a4504cdcf94cc88d045cb5f99c581f3b638e5ae0c896848a9cc51a08f0d18082583900c408ec82820b576b7a15b98c440557d954f2667973e633015eafe050af4324e5bd3b36ff582e1b18776c90b78977dd7dfd30f70055b926ce1a0a21fe80825839003303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92c44b606f69e387098b206e20c78ccfb167eaf42826ce81697e84fb781ad1e3dddd021a0003226d031a02b7a5df0e82581c3303d31630bdf68f784648498c5175ed3e9c2d270a137af0f225ae92581c5f8c24ba455f9ad1cbc14e8acd8d30d06735c47e2a4dfc202a0235c2a20081825820f48c1363aa87e177789dead3cc9c354ee4675e8c53f560e5eedb587ebb4fc3b45840f9e6f1a4ed900078a02579531336586424a886f3f4b4f761b16ca681659b60ca256e387f2a81a890d85546d2e4e49378bd455e90ef97091c4fcd90172ad14609049ffff5f6";
                // const serializedCBOR = await wallet.signTx(apolloCBOR, true);

                // const txHash = await wallet.submitTx(serializedCBOR);
                // console.log(txHash);
            } else {
                reset();
            }
        })();
    }, [selectedWallet]);

    function handleError(error) {
        const errorMessage = error.info || error.message || error.toString();
        setMessage(errorMessage, "error");
        reset();
        return;
    }

    async function isSubmitted(txHash, result) {
        if (result) {
            setMessage(`Transaction submitted successfully`, "update");
            await sleep(3000);
            setMessage(`TxHash: ${txHash}`, "update");
            await sleep(3000);
            return;
        } else {
            setMessage(`Transaction Submission Failed`, "error");
            reset();
            return;
        }
    }

    const getTotalAda = (utxos) => {
        let totalLovelace = 0;

        for (const utxo in utxos) {
            let assets = utxos[utxo].assets;

            Object.keys(assets).map((asset) => {
                if (asset == "lovelace") totalLovelace += Number(assets[asset]);
            });
        }

        return (totalLovelace / 1000000).toFixed(2);
    };

    const reset = (all = false) => {
        if (all) setMessage("", "update");

        setLoading(false);
        setSelectedWallet(null);
        setAPI(null);
        setFunds(0);
    };

    const verifyWallet = async () => {
        const recipient = await lucid.wallet.address();

        const payload = makeHexID(100);

        const verify = await lucid
            .newMessage(recipient, payload)
            .sign()
            .then(async (message) => {
                const { status_code, jwt } = await requestJWT(recipient, payload, message);
                if (status_code != 200) return false;
                setJWT(jwt);
                return { address: recipient, jwt: jwt };
            })
            .catch((error) => {
                console.log(error);
                return false;
            });

        return verify;
    };

    return (
        <div
            className={`justify-center text-center relative font-main text-main-text lowercase grid pointer-events-auto`}
        >
            <div className={`flex justify-end w-full ${api == null ? "max-w-[300px]" : "max-w-[50%] min-w-[300px]"} `}>
                <WalletInfo wallet={selectedWallet} api={api} funds={funds} reset={reset} />
            </div>
            <div
                className={`grid border-main-primary  border-2 transition-all
                     bg-main-primary bg-opacity-90 z-10 ${api != null && selectedWallet != null ? "hidden" : "visible"}
                      max-w-[95vw] min-w-[300px]`}
            >
                <div
                    className={`flex flex-col items-center
                     row-start-1 row-end-2 col-start-1 col-end-2
                     transition-opacity duration-500 ease-in-out
                     ${api == null ? `opacity-100 z-10 visible` : `opacity-0 z-0 hidden`}`}
                >
                    <WalletSelection selectedWallet={selectedWallet} setSelectedWallet={setSelectedWallet} />
                </div>
                <div
                    className={`flex justify-center items-center
                     row-start-1 row-end-3 col-start-1 col-end-2
                     transition-opacity duration-500 ease-in-out bg-black bg-opacity-75
                     ${loading ? `opacity-100 z-20` : `opacity-0 z-0`}`}
                >
                    <Loading message={loadingMessage} />
                </div>
            </div>
            <div
                className={`grid grid-flow-col text-center ${
                    api == null ? "max-w-[300px]" : "max-w-[50%] min-w-[300px]"
                } `}
            >
                <Status statusMessage={statusMessage} />
            </div>
        </div>
    );
}
